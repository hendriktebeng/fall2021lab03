// Hendrik Tebeng - 2035508
package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {
    // Get methods tests for the Vector 3d class
    @Test
    public void testGet(){
        Vector3d vector = new Vector3d(3,5,2);
        assertEquals(3, vector.getX());
        assertEquals(5, vector.getY());
        assertEquals(2, vector.getZ());
    }
    // magnitude() test for the Vector 3d class
    @Test
    public void testMagnitude(){
        Vector3d vector = new Vector3d(3,5,2);
        assertEquals(6.16441400297, vector.magnitude(), 0.000001);
    }
    // dotProduct() test for the Vector 3d class
    @Test
    public void testDotProduct(){
        Vector3d vector = new Vector3d(3,5,2);
        Vector3d secondVector = new Vector3d(2,1,6);
        assertEquals(23.0, vector.dotProduct(secondVector));
    }
    // add() methods test for the Vector 3d class
    @Test
    public void testAdd(){
        Vector3d vector = new Vector3d(3,5,2);
        Vector3d secondVector = new Vector3d(2,1,6);
        Vector3d newVector = vector.add(secondVector);
        assertEquals(5, newVector.getX());
        assertEquals(6, newVector.getY());
        assertEquals(8, newVector.getZ());
    }
}
